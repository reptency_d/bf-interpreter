# Brainfuck Interpreter

A Haskell interpreter for Brainfuck language.


## Usage

Import the file using GHCi, the run: `interactive_eval <code> <input_buffer> <cell_of_the_tape_shown>`
