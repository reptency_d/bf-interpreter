module BrainFuck where

import Data.Char

-- consumed source / source to consume / parenthesis level / IO / pre tape / post tape
compute :: String -> String -> Int -> (String, String) -> [Int] -> [Int] -> ([Int], (String, String))

compute pre_code "" lv (inp, outp) pre_tape post_tape = case lv of
    0 -> (reverse_and_concatenate pre_tape post_tape, (inp, outp))
    _ ->  error ("Failure: unclosed [], char pos = " ++ ((show . length) pre_code) ++ " lv = " ++ (show lv))

compute pre_code (c:post_code) lv (inp, outp) pre_tape (t:post_tape) = case c of
    '>' -> case post_tape of
        [] -> error ("Failure: stepping outside the tape (forward), char pos = " ++  ((show . length) pre_code))
        (p:pre_tape_rest) -> compute (c:pre_code) post_code lv (inp, outp) (t:pre_tape) post_tape
    '<' -> case pre_tape of
        [] -> error ("Failure: stepping outside the tape (backward), char pos = " ++ ((show . length) pre_code))
        (p:pre_tape_rest) -> compute (c:pre_code) post_code lv (inp, outp) pre_tape_rest (p:t:post_tape)
    '+' -> compute (c:pre_code) post_code lv (inp, outp) pre_tape ((op_256 (+  1 ) t):post_tape)
    '-' -> compute (c:pre_code) post_code lv (inp, outp) pre_tape ((op_256 (+(-1)) t):post_tape)
    '.' -> compute (c:pre_code) post_code lv (inp, (chr t):outp) pre_tape (t:post_tape)
    ',' -> case inp of
        []    -> error ("Failure: Failure: empty input buffer")
        (i:inp_rest) -> compute (c:pre_code) post_code lv (inp_rest, outp) pre_tape ((ord i) : post_tape)
    '[' -> case t of
        0 -> compute f s lv (inp, outp) pre_tape (t:post_tape)
            where (f, s) = forwroll_with_levels (c:pre_code) (post_code) 1 -- Important, '[' is already read
        _ -> compute (c:pre_code) post_code (lv+1) (inp, outp) pre_tape (t:post_tape)
    ']' -> compute f s (lv-1) (inp, outp) pre_tape (t:post_tape)
        where (f, s) = backroll_with_levels pre_code (c:post_code) 1


reverse_and_concatenate :: [a] -> [a] -> [a]
reverse_and_concatenate [] r = r
reverse_and_concatenate (x:xs) r = reverse_and_concatenate xs (x:r)


backroll_with_levels :: String -> String -> Int -> (String, String)
backroll_with_levels pre post 0 = (pre, post)
backroll_with_levels "" post lv = error "parenthesis stack smashed"
backroll_with_levels (c:cpre) post lv = case c of
    ']' -> backroll_with_levels cpre (c:post) (lv+1)
    '[' -> backroll_with_levels cpre (c:post) (lv-1)
    _   -> backroll_with_levels cpre (c:post) lv

forwroll_with_levels :: String -> String -> Int -> (String, String)
forwroll_with_levels pre post 0 = (pre, post)
forwroll_with_levels post "" lv = error "parenthesis stack smashed"
forwroll_with_levels pre (c:cpost) lv = case c of
    ']' -> forwroll_with_levels (c:pre) cpost (lv-1)
    '[' -> forwroll_with_levels (c:pre) cpost (lv+1)
    _   -> forwroll_with_levels (c:pre) cpost lv

op_256 :: (Int -> Int) -> Int -> Int
op_256 f n = (f n) `mod` 256



eval :: String -> [Int] -> ([Int], (String, String))
eval code tape = compute "" code 0 ("", "") [] tape

view_tape :: Int -> ([Int], (String, String)) -> [Int]
view_tape n xs = take n (fst xs)

view_output :: ([Int], (String, String)) -> String
view_output = (reverse . snd . snd)


interactive_eval :: String -> String -> Int -> ([Int], String)
interactive_eval code inp out_len = (out_tape, output)
    where out_tape = view_tape out_len result
          output   = view_output result
          result   = compute "" code 0 (inp, "") [] (repeat 0)


-- usage: interactive_eval "++++++++++[>+++++++>++++++++++>+++>+<<<<-]>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>." "" 20
